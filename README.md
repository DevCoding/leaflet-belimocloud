# BelimoCloudWorldView

Device management on the map using Leaflet library

# Tasks/Goals

- Improve grouping / flexible selection of products on the map
- Filter by device health, device type [out of scope: and last seen timestamp]
- Clean up js code, add documentation
- Make code more robust / generic

# Initial Situations

- Existing, so called world map view with clickable dots that show device information
- Three product types are listed
- The data source is a geojson (static, updated every 24h, not in scope)
- Small legend in the bottom right
- Embedded in an interface with three tabs (relevant is only the world map so far)
- The code for gathering the json data is run on a server as a python script (out of scope so far)
- The leaflet is already using the following plugins:
    - Marker Cluster (two switchable layers)
    - Groupedlayercontrol

### Installation

- Node
    ```sh
    $ cd Leaflet-BelimoCloud
    $ npm install
    $ npm run start
    ```
- Python
    - If Python version returned above is 3.X
        ```sh
        $ cd Leaflet-BelimoCloud
        $ python -m http.server
        ```
    - If Python version returned above is 2.X
        ```sh
        $ cd Leaflet-BelimoCloud
        $ python -m SimpleHTTPServer
        ```